﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mkr2dp
{   /*9.Розробити додаток для керування звуковою системою в домашньому кінотеатрі.
    Користувач має можливість увімкнути чи вимкнути звук та змінити гучність.
    Кожна з цих дій може бути виконана після натискання відповідної кнопки на
    пульті дистанційного керування або передана через інтернет з мобільного
    додатку.Після виклику кожної команди виводити в консоль гучність системи.*/
    //Застосовано шаблон "Команда" (Command Pattern)
    public interface ICommand
    {
        void Execute();
    }

    public class TurnOnCommand : ICommand
    {
        private readonly AudioSystem audioSystem;

        public TurnOnCommand(AudioSystem audioSystem)
        {
            this.audioSystem = audioSystem;
        }

        public void Execute()
        {
            audioSystem.TurnOn();
        }
    }

    public class TurnOffCommand : ICommand
    {
        private readonly AudioSystem audioSystem;

        public TurnOffCommand(AudioSystem audioSystem)
        {
            this.audioSystem = audioSystem;
        }

        public void Execute()
        {
            audioSystem.TurnOff();
        }
    }

    public class SetVolumeCommand : ICommand
    {
        private readonly AudioSystem audioSystem;
        private readonly int volume;

        public SetVolumeCommand(AudioSystem audioSystem, int volume)
        {
            this.audioSystem = audioSystem;
            this.volume = volume;
        }

        public void Execute()
        {
            audioSystem.SetVolume(volume);
        }
    }

    public class AudioSystem
    {
        private int volume = 50; 

        public void TurnOn()
        {
            Console.WriteLine("Звукова система увімкнена");
            ShowVolume();
        }

        public void TurnOff()
        {
            Console.WriteLine("Звукова система вимкнена");
        }

        public void SetVolume(int volume)
        {
            this.volume = volume;
            Console.WriteLine($"Гучність: {this.volume}");
        }

        private void ShowVolume()
        {
            Console.WriteLine($"Поточна гучність: {volume}");
        }
    }

    public class RemoteControl
    {
        private ICommand command;

        public void SetCommand(ICommand command)
        {
            this.command = command;
        }

        public void PressButton()
        {
            if (command != null)
            {
                command.Execute();
            }
        }
    }

    class Program
    {
        static void Main()
        {
            AudioSystem audioSystem = new AudioSystem();
            RemoteControl remote = new RemoteControl();

            remote.SetCommand(new TurnOnCommand(audioSystem));
            remote.PressButton();

            remote.SetCommand(new SetVolumeCommand(audioSystem, 70));
            remote.PressButton();

            remote.SetCommand(new TurnOffCommand(audioSystem));
            remote.PressButton();

            remote.SetCommand(new SetVolumeCommand(audioSystem, 40));
            remote.PressButton(); 
        }
    }
}
